export class CreatePostDto {
  text?: string;
  imageLink?: string;
  authorId: string;
}
