import { Column, PrimaryGeneratedColumn, Entity } from 'typeorm';

interface IPost {
  id?: number;
  text?: string;
  imageLink?: string;
  authorId?: string;
  userLools?: string[];
}

@Entity()
export class Post {
  constructor(iPost?: IPost) {
    if (iPost) {
      if (iPost.id) this.id = iPost.id;
      if (iPost.authorId) this.authorId = iPost.authorId;
      if (iPost.text) this.text = iPost.text;
      if (iPost.imageLink) this.imageLink = iPost.imageLink;
      if (iPost.userLools) this.userLools = iPost.userLools;
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  authorId: string;

  @Column({ nullable: true })
  text: string;

  @Column({ nullable: true })
  imageLink: string;

  @Column('text', { array: true, default: [] })
  userLools: string[];
}
