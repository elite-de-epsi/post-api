FROM node:14-alpine

EXPOSE 3000

WORKDIR /usr/src/app

COPY package*.json ./

RUN rm package-lock.json

RUN npm install

COPY . .

RUN npm run build

CMD ["node", "dist/main"]